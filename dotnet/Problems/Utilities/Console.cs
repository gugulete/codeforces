﻿using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text;
using Math = System.Math;
using Random = System.Random;

namespace Utilities {

    class Console {
        private static List<string> lines;
        private static int current;
        private static Random random = new Random (10);

        static Console () {
            current = 0;
            lines = new List<string> ();

            ReadFormFile ();
            // CustomInput ();
        }

        public static void Write (object str) {
            System.Console.Write (str);
        }

        public static void WriteLine (object str) {
            System.Console.WriteLine (str);
        }

        public static string ReadLine () {
            return lines[current++];
        }

        private static void ReadFormFile () {
            string dir = Path.GetDirectoryName (Assembly.GetExecutingAssembly ().Location);
            string inputPath = $"{dir}/../../../input.txt";
            using (var sr = new StreamReader (inputPath)) {
                string line;
                while ((line = sr.ReadLine ()) != null) {
                    lines.Add (line);
                }
            }
        }

        private static void CustomInput () {

        }
    }
}