﻿using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace Utilities {

    static class Extensions {
        public static void Print (this IEnumerable enumerable) {
            foreach (var e in enumerable) {
            Console.Write (e + " ");
            }
            Console.WriteLine (" ");
        }

        public static string GetString (this IEnumerable enumerable) {
            var s = new StringBuilder ();
            foreach (var e in enumerable) {
            s.Append (e + " ");
            }
            return s.ToString ().Trim ();
        }

        public static void Print (this IDictionary dictionary) {
            foreach (var e in dictionary) {
                Console.WriteLine (((DictionaryEntry) e).Value.ToString ());
            }
            Console.WriteLine (" ");
        }
    }
}