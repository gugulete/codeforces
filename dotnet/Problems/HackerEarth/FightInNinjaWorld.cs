using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Console = Utilities.Console;

// https://www.hackerearth.com/practice/algorithms/graphs/breadth-first-search/practice-problems/algorithm/containers-of-choclates-1/

namespace HackerEarth {
    static class FightInNinjaWorld {

        static void Main1 (string[] args) {
            var testsCount = int.Parse (Console.ReadLine ());
            for (int i = 1; i <= testsCount; i++) {
                Run (i);
            }
        }

        private static void Run (int testNumber) {
            var fightsCount = int.Parse (Console.ReadLine ());
            var fights = new FightsGraph ();
            for (int i = 0; i < fightsCount; i++) {
                var inp = Console.ReadLine ().Split (' ');
                fights.AddFight (int.Parse (inp[0]), int.Parse (inp[1]));
            }

            var sum = fights.Nodes.GroupBy (n => n.Value.Group).Select (g => {
                var total = g.Count ();
                var count = g.Count (x => x.Value.Type == 0);
                return Math.Max (total - count, count);
            }).Sum ();

            Console.WriteLine ($"Case {testNumber}: {sum}");
        }
    }

    public class NodeData {
        public HashSet<int> Neighbours;
        public int Type;
        public int Group;
        public int Value { get; }

        public NodeData (int value, int type, int group) {
            this.Value = value;
            this.Type = type;
            this.Group = group;
            this.Neighbours = new HashSet<int> ();
        }
    }

    public class FightsGraph {
        public SortedDictionary<int, NodeData> Nodes { get; }

        public FightsGraph () {
            this.Nodes = new SortedDictionary<int, NodeData> ();
        }

        int maxGroup = -1;

        public void AddFight (int u, int v) {
            NodeData nodeU, nodeV;
            if (Nodes.ContainsKey (u)) {
                nodeU = Nodes[u];
                if (Nodes.ContainsKey (v)) {
                    nodeV = Nodes[v];

                    if (nodeU.Group != nodeV.Group) {
                        UpdateGroups (nodeV, nodeU.Group);
                    }

                    if (nodeU.Type == nodeV.Type) {
                        UpdateTypes (nodeV, nodeV.Type ^ 1);
                    }
                } else {
                    nodeV = new NodeData (v, nodeU.Type ^ 1, nodeU.Group);
                    Nodes[v] = nodeV;
                }
            } else {
                if (Nodes.ContainsKey (v)) {
                    nodeV = Nodes[v];
                    nodeU = new NodeData (u, nodeV.Type ^ 1, nodeV.Group);
                    Nodes[u] = nodeU;
                } else {
                    maxGroup++;
                    nodeU = new NodeData (u, 1, maxGroup);
                    nodeV = new NodeData (v, 0, maxGroup);
                    Nodes[v] = nodeV;
                    Nodes[u] = nodeU;
                }
            }

            nodeU.Neighbours.Add (v);
            nodeV.Neighbours.Add (u);
        }

        public class UpdateOperation {
            public readonly HashSet<int> set;
            public readonly int value;

            public UpdateOperation (int value, HashSet<int> set) {
                this.value = value;
                this.set = set;
            }
        }

        private void UpdateGroups (NodeData startNode, int group) {

            Queue<HashSet<int>> executionQueue = new Queue<HashSet<int>> ();

            executionQueue.Enqueue (new HashSet<int> { startNode.Value });

            while (executionQueue.Count > 0) {
                var layer = executionQueue.Dequeue ();
                foreach (var value in layer) {
                    var node = Nodes[value];
                    var result = UpdateNodeGroup (node, group);
                    if (result != null) {
                        executionQueue.Enqueue (result);
                    }
                }
            }
        }

        private void UpdateTypes (NodeData startNode, int type) {

            Queue<UpdateOperation> executionQueue = new Queue<UpdateOperation> ();

            executionQueue.Enqueue (new UpdateOperation (type, new HashSet<int> { startNode.Value }));

            while (executionQueue.Count > 0) {
                var layer = executionQueue.Dequeue ();
                foreach (var value in layer.set) {
                    var node = Nodes[value];
                    var result = UpdateNodeType (node, layer.value);
                    if (result != null) {
                        executionQueue.Enqueue (new UpdateOperation (layer.value ^ 1, result));
                    }
                }
            }
        }

        private HashSet<int> UpdateNodeGroup (NodeData node, int group) {
            if (node.Group == group) {
                return null;
            }

            node.Group = group;
            return node.Neighbours;
        }

        private HashSet<int> UpdateNodeType (NodeData node, int type) {
            if (node.Type == type) {
                return null;
            }

            node.Type = type;
            return node.Neighbours;
        }
    }
}