using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Console = Utilities.Console;

// https://www.hackerearth.com/practice/algorithms/graphs/breadth-first-search/practice-problems/algorithm/containers-of-choclates-1/

namespace HackerEarth {

    static class FightInNinjaWorld1 {

        static void Main1 (string[] args) {
            var testsCount = int.Parse (Console.ReadLine ());
            for (int i = 1; i <= testsCount; i++) {
                Console.WriteLine ($"Case {i}: {new Runner ().Result}");
            }
        }
    }

    class Runner {
        SortedSet<int> fighters;
        SortedSet<int>[] fights;
        int[] visited;
        int count0;
        int count1;
        int count;

        public int Result { get => this.count; }

        public Runner () {
            this.fighters = new SortedSet<int> ();
            this.fights = new SortedSet<int>[100000];
            this.visited = new int[100000];
            this.count = 0;

            var fightsCount = int.Parse (Console.ReadLine ());

            while (fightsCount-- > 0) {
                var inp = Console.ReadLine ().Split (' ');
                var x = int.Parse (inp[0]);
                var y = int.Parse (inp[1]);

                this.fighters.Add (x);
                this.fighters.Add (y);

                GetFights (x).Add (y);
                GetFights (y).Add (x);
            }

            foreach (var fighter in this.fighters) {
                if (this.visited[fighter] == 0) {
                    this.count0 = 0;
                    this.count1 = 0;

                    this.DFS (fighter, 1);

                    this.count += Math.Max (this.count0, this.count1);
                }
            }
        }

        private void DFS (int start, int type) {
            this.visited[start] = 1;

            int opponentType;
            if (type == 0) {
                this.count0++;
                opponentType = 1;
            } else {
                this.count1++;
                opponentType = 0;
            }

            foreach (var opponent in this.fights[start]) {
                if (this.visited[opponent] == 0) {
                    this.DFS (opponent, opponentType);
                }
            }
        }

        private SortedSet<int> GetFights (int x) {
            if (this.fights[x] == null) {
                this.fights[x] = new SortedSet<int> ();
            }

            return this.fights[x];
        }
    }
}