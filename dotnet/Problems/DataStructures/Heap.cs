using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace DataStructures {
    public class Heap<T> : IEnumerable<T> {
        private readonly List<T> nodes;
        private readonly Comparison<T> comparison;

        /// <summary>
        /// The count of the elements in the heap.
        /// </summary>
        public int Count {
            get {
                return this.nodes.Count;
            }
        }

        /// <summary>
        /// Initializes a new heap instance.
        /// </summary>
        /// <param name="comparison">Compares parent to child value.</param>
        public Heap (Comparison<T> comparison) {
            this.comparison = comparison;
            this.nodes = new List<T> ();
        }

        /// <summary>
        /// Adds new element to the heap.
        /// </summary>
        public void Enqueue (T element) {
            var index = nodes.Count;
            nodes.Add (element);

            var parent = this.GetParent (index);
            while (parent != index &&
                this.Compare (parent, index) < 0) {
                Swap (parent, index);

                index = parent;
                parent = this.GetParent (index);
            }
        }

        /// <summary>
        /// Removes the top element of the heap and returns it.
        /// </summary>
        /// <returns>The top element.</returns>
        public T Dequeue () {
            if (nodes.Count == 0) {
                throw new InvalidOperationException ("no elements in heap");
            }
            var last = nodes.Count - 1;
            this.Swap (0, last);

            var element = nodes[last];
            nodes.RemoveAt (last);

            if (nodes.Count > 0) {
                var parent = 0;

                while (parent >= 0 && parent < nodes.Count) {
                    var leftChild = this.GetLeftChild (parent);
                    var rightChild = this.GetRightChild (parent);

                    var child = -1;
                    if (leftChild < nodes.Count && this.Compare (parent, leftChild) < 0) {
                        child = leftChild;
                    }

                    if (rightChild < nodes.Count && this.Compare (parent, rightChild) < 0) {
                        if (child < 0 || this.Compare (child, rightChild) < 0) {
                            child = rightChild;
                        }
                    }

                    if (child >= 0) {
                        this.Swap (parent, child);
                        parent = child;
                    }

                    parent = child;
                }
            }

            return element;
        }

        public override string ToString () {
            var sb = new StringBuilder ($"count: {this.Count}\n");
            this.PrintDfs (0, 0, sb);
            return sb.ToString ();
        }

        private int Compare (int index1, int index2) {
            return comparison (nodes[index1], nodes[index2]);
        }

        private void Swap (int index1, int index2) {
            var element1 = nodes[index1];
            var element2 = nodes[index2];

            nodes[index1] = element2;
            nodes[index2] = element1;
        }

        private int GetParent (int index) {
            return (index - 1) / 2;
        }

        private int GetLeftChild (int index) {
            return 2 * index + 1;
        }

        private int GetRightChild (int index) {
            return 2 * index + 2;
        }

        private void PrintDfs (int index, int level, StringBuilder s) {
            if (index >= nodes.Count) {
                return;
            }

            s.Append (new String ('\t', level));
            s.AppendLine (nodes[index].ToString ());

            PrintDfs (this.GetLeftChild (index), level + 1, s);
            PrintDfs (this.GetRightChild (index), level + 1, s);
        }

        IEnumerator<T> IEnumerable<T>.GetEnumerator () {
            return this.nodes.GetEnumerator ();
        }

        IEnumerator IEnumerable.GetEnumerator () {
            return this.nodes.GetEnumerator ();
        }
    }
}