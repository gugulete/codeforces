﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Console = Utilities.Console;

namespace Contest_913 {

    static class TooEasyProblems {

        static void Main1 (string[] args) {
            var line = Console.ReadLine ().Split (' ');

            var n = int.Parse (line[0]);
            var timeLimit = long.Parse (line[1]);

            var problems = new SortedDictionary<int, ProblemData> ();
            ProblemData.TimeLimit = timeLimit;

            for (int i = 1; i <= n; i++) {
                line = Console.ReadLine ().Split (' ');
                var problemsLimit = int.Parse (line[0]);
                var time = int.Parse (line[1]);

                if (problems.ContainsKey (problemsLimit)) {
                    var data = problems[problemsLimit];
                    data.AddSortedTime (time, i);
                } else if (time <= timeLimit) {
                    problems[problemsLimit] = new ProblemData (problemsLimit, time, i);
                }
            }

            // end input

            long maxScore = 0;
            int maxCount = 0;
            List<int> maxIndices = null;

            while (problems.Any ()) {
                var data = problems.First ().Value;
                data.ClearSelf ();

                var skipTimes = 0;
                while (data.ProblemsCapacity > 0 && data.TimeCapacity > 0) {
                    if (ProblemData.SortedTimesMap.Count <= skipTimes) {
                        break;
                    }

                    var nextData = ProblemData.SortedTimesMap.Skip (skipTimes).First ();

                    var count = nextData.Value.Count;
                    var time = nextData.Key;

                    var take = (int) Math.Min (count, Math.Min (data.ProblemsCapacity, data.TimeCapacity / time));

                    if (take == 0) {
                        break;
                    }

                    skipTimes++;

                    for (int i = 0; i < take; i++) {
                        data.Times.AddUnsorted (nextData.Value[i], nextData.Value.Indices[i]);
                    }
                    data.TimeCapacity -= time * take;
                }

                var score = data.Times.Count;
                if (score > maxScore) {
                    maxScore = score;
                    maxCount = data.ProblemsLimit - data.ProblemsCapacity;
                    maxIndices = data.Times.Indices;
                }

                problems.Remove (data.ProblemsLimit);

            }

            Console.WriteLine (maxScore);
            Console.WriteLine (maxCount);
            var sb = new StringBuilder ();
            if (maxIndices != null) {
                foreach (var i in maxIndices) {
                    sb.Append (i).Append (" ");
                }
            }

            Console.WriteLine (sb.ToString ().TrimEnd ());
        }
    }

    public class RSortedList : List<int> {
        public RSortedList () { }

        public List<int> Indices { get; } = new List<int> ();

        public void AddSorted (int item, int index) {
            if (this.Count == 0) {
                this.Add (item);
                this.Indices.Add (index);
            } else {
                var position = this.FindInsertIndex (item);
                this.Insert (position, item);
                this.Indices.Insert (position, index);
            }
        }

        public struct Replaced {
            public int position;
            public int item;

            public Replaced (int position, int item) {
                this.position = position;
                this.item = item;
            }
        }

        public Replaced? ReplaceSorded (int item, int index) {
            var position = this.FindInsertIndex (item);
            if (position < this.Count) {
                var replaced = this [this.Count - 1];

                this.RemoveAt (this.Count - 1);
                this.Indices.RemoveAt (this.Indices.Count - 1);

                this.Insert (position, item);
                this.Indices.Insert (position, index);

                return new Replaced (position, replaced);
            }

            return null;
        }

        private int FindInsertIndex (int item) {
            var start = 0;
            var end = this.Count - 1;
            var mid = (end + start) / 2;

            do {
                if (item >= this [end]) {
                    return end + 1;
                }

                if (item <= this [start]) {
                    return start;
                }

                if (item == this [mid]) {
                    return mid;
                }

                if (item < this [mid]) {
                    end = Math.Max (start, mid - 1);
                } else {
                    start = Math.Min (end, mid + 1);
                }

                mid = (end + start) / 2;

            } while (start <= end);

            return mid;
        }

        private int FindItemIndex (int item) {
            var start = 0;
            var end = this.Count - 1;
            var mid = (end + start) / 2;

            if (item > this [end] || item < this [start]) {
                return -1;
            }

            do {
                if (item == this [end]) {
                    return end;
                }

                if (item == this [start]) {
                    return start;
                }

                if (item == this [mid]) {
                    return mid;
                }

                if (item < this [mid]) {
                    end = Math.Max (start, mid - 1);
                } else {
                    start = Math.Min (end, mid + 1);
                }

                mid = (end + start) / 2;

            } while (start <= end);

            return -1;
        }

        public void RemoveSorded (int item) {
            var index = this.FindItemIndex (item);
            this.RemoveAt (index);
            this.Indices.RemoveAt (index);
        }

        public void AddUnsorted (int time, int index) {
            this.Add (time);
            this.Indices.Add (index);
        }
    }

    class ProblemData {
        public static SortedDictionary<int, RSortedList> SortedTimesMap = new SortedDictionary<int, RSortedList> ();

        public static long TimeLimit;
        public long TimeCapacity;
        public int ProblemsLimit;
        public RSortedList Times;

        public int ProblemsCapacity { get { return this.ProblemsLimit - this.Times.Count; } }

        public ProblemData (int problemsLimit, int time, int index) {
            if (time > TimeLimit) {
                throw new Exception ("time should not exceed timeLimit");
            }

            this.ProblemsLimit = problemsLimit;

            this.Times = new RSortedList ();
            this.Times.AddSorted (time, index);
            this.TimeCapacity = TimeLimit - time;

            this.TimesMapAdd (time, index);
        }

        public void AddSortedTime (int time, int index) {
            if (Times.Count == ProblemsLimit) {
                this.ReplaceTime (time, index);
            } else {
                this.InsertTime (time, index);
            }
        }

        public void ClearSelf () {
            for (int i = 0; i < this.Times.Count; i++) {
                this.TimesMapRemove (this.Times[i], this.Times.Indices[i]);
            }
        }

        private void InsertTime (int time, int index) {
            if (this.TimeCapacity >= time) {
                this.Times.AddSorted (time, index);
                this.TimeCapacity -= time;

                this.TimesMapAdd (time, index);
            } else {
                this.ReplaceTime (time, index);
            }
        }

        private void ReplaceTime (int time, int index) {
            var replaced = this.Times.ReplaceSorded (time, index);
            if (replaced.HasValue) {
                this.TimeCapacity += replaced.Value.item;
                this.TimeCapacity -= time;

                this.TimesMapRemove (replaced.Value.item, replaced.Value.position);
                this.TimesMapAdd (time, index);
            }
        }

        private void TimesMapAdd (int time, int index) {
            if (!SortedTimesMap.ContainsKey (time)) {
                SortedTimesMap[time] = new RSortedList ();
            }

            SortedTimesMap[time].AddSorted (this.ProblemsLimit, index);
        }

        private void TimesMapRemove (int time, int index) {
            if (!SortedTimesMap.ContainsKey (time)) {
                return;
            }

            SortedTimesMap[time].RemoveSorded (this.ProblemsLimit);
            if (SortedTimesMap[time].Count == 0) {
                SortedTimesMap.Remove (time);
            }
        }
    }
}