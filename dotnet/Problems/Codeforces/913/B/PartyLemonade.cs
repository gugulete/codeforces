﻿using System;
using System.Collections.Generic;
using System.Linq;
using Console = Utilities.Console;

namespace Contest_913 {

    static class PartyLemonade {

        static void Main1 (string[] args) {
            var line = Console.ReadLine ().Split (' ');

            var n = long.Parse (line[0]);
            var L = long.Parse (line[1]);

            var costs = Console.ReadLine ().Split (' ').Select (selector: int.Parse).ToList ();

            var leftMins = new long[n];
            var rightMins = new long[n];

            leftMins[0] = costs[0];
            var prevCost = leftMins[0];
            for (var i = 1; i < n; i++) {
                leftMins[i] = Math.Min (costs[i], 2 * prevCost);
                prevCost = leftMins[i];
            }

            rightMins[n - 1] = leftMins[n - 1];
            for (var i = n - 2; i >= 0; i--) {
                rightMins[i] = Math.Min (leftMins[i], rightMins[i + 1]);
            }

            Console.WriteLine (FindMin (L, leftMins, rightMins));
        }

        static long FindMin (long value, long[] leftMins, long[] rightMins) {
            var index = Math.Min (leftMins.Length - 1, (int) Math.Floor (Math.Log (value, 2)));
            var rightMin = FindRightMin (index + 1, rightMins);
            var leftMin = FindLeftMin (value, index, leftMins, rightMins);

            return Math.Min (rightMin, leftMin);
        }

        static long FindLeftMin (long value, int index, long[] leftMins, long[] rightMins) {
            long count = value >> index;
            if (count == 0) {
                throw new Exception ("oh");
            }
            var cost = count * leftMins[index];
            value -= count << index;

            if (value == 0) {
                return cost;
            }

            return cost + FindMin (value, leftMins, rightMins);
        }

        static long FindRightMin (int index, long[] rightMins) {
            if (rightMins.Length > index) {
                return rightMins[index];
            }

            return long.MaxValue;
        }
    }
}