using System;
using System.Linq;
using Console = Utilities.Console;

namespace Contest_319 {

    class Psycho {
        public int Value;
        public Psycho KilledBy = null;
        public int DiesAt = 0;

        public Psycho (int value) {
            this.Value = value;
        }

        public bool Kills (Psycho p) {
            return this.Value > p.Value;
        }
    }

    static class PsychosInALine {
        static void Main1 (string[] args) {
            var n = int.Parse (Console.ReadLine ());
            var input = Console.ReadLine ().Split (' ').Select (int.Parse);

            var psychos = new Psycho[n];
            var i = 0;
            var maxStage = 0;
            foreach (var value in input) {
                psychos[i] = new Psycho (value);

                if (i - 1 >= 0) {
                    FindKiller (psychos[i], psychos[i - 1], 1);
                }

                if (psychos[i].DiesAt > maxStage) {
                    maxStage = psychos[i].DiesAt;
                }

                i++;
            }

            Console.WriteLine (maxStage);
        }

        static void FindKiller (Psycho psycho, Psycho killer, int stage) {
            if (killer == null) {
                return;
            }

            if (killer.Kills (psycho)) {
                if (killer.KilledBy == null || killer.DiesAt >= stage) {
                    psycho.KilledBy = killer;
                    psycho.DiesAt = stage;
                } else if (killer.DiesAt < stage) {
                    FindKiller (psycho, killer.KilledBy, stage);
                }
            } else {
                FindKiller (psycho, killer.KilledBy, killer.DiesAt + 1);
            }
        }
    }
}