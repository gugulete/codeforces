using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataStructures;
using Console = Utilities.Console;

namespace Contest_960 {
    static class MinimizeTheError {
        static void Main1 (string[] args) {
            var prms = Console.ReadLine ().Split (' ');
            var n = int.Parse (prms[0]);
            var k = int.Parse (prms[1]) + int.Parse (prms[2]);
            var a = Console.ReadLine ().Split (' ');
            var b = Console.ReadLine ().Split (' ');

            var diffs = new Heap<long> ((x, y) => Math.Abs (x).CompareTo (Math.Abs (y)));
            for (var i = 0; i < n; i++) {
                diffs.Enqueue (long.Parse (a[i]) - long.Parse (b[i]));
            }

            while (k > 0) {
                var element = diffs.Dequeue ();
                if (element >= 0) {
                    element -= 1;
                } else {
                    element += 1;
                }
                diffs.Enqueue (element);
                k--;
            }

            var sum = diffs.Sum (x => x * x);

            Console.WriteLine (sum);
        }
    }
}