using System;
using System.Linq;
using System.Text;
using Console = Utilities.Console;

namespace Contest315 {

    static class ProblemB {

        static void Main1 (string[] args) {
            var m = int.Parse (Console.ReadLine ().Split (' ') [1]);

            var a = Console.ReadLine ().Split (' ').Select (int.Parse).ToList ();

            var sb = new StringBuilder ();
            var increment = 0;

            for (int i = 0; i < m; i++) {
                var inp = Console.ReadLine ().Split (' ').Select (int.Parse).ToList ();
                switch (inp[0]) {
                    case 1:
                        a[inp[1] - 1] = inp[2] - increment;
                        break;
                    case 2:
                        increment += inp[1];
                        break;
                    default:
                        sb.AppendLine ((a[inp[1] - 1] + increment).ToString ());
                        break;
                }
            }

            Console.WriteLine (sb.ToString ());
        }
    }
}