using System;
using System.Linq;
using Console = Utilities.Console;

namespace Contest_279 {
    static class Ladder {
        static void Main1 (string[] args) {
            var prms = Console.ReadLine ().Split (' ');
            var n = int.Parse (prms[0]);
            var q = int.Parse (prms[1]);
            var input = Console.ReadLine ().Split (' ').Select (int.Parse).ToArray ();

            var intervals = GetIntervals (input);

            for (int i = 0; i < q; i++) {
                var query = Console.ReadLine ().Split (' ');
                var from = int.Parse (query[0]) - 1;
                var to = int.Parse (query[1]) - 1;
                Console.WriteLine (from >= intervals[to] ? "Yes" : "No");
            }
        }

        private static int[] GetIntervals (int[] a) {
            var result = new int[a.Length];
            result[0] = 0;

            var from = 0;
            var flatFrom = 0;
            var flatTo = 0;

            var i = 1;
            var direction = 0;

            while (i < a.Length) {
                switch (a[i].CompareTo (a[i - 1])) {
                    case 0:
                        result[i] = from;

                        if (flatTo + 1 == i) {
                            flatTo = i;
                        } else {
                            flatFrom = i - 1;
                            flatTo = i;
                        }

                        break;
                    case 1:
                        if (direction >= 0) {
                            // continue going up
                            result[i] = from;
                        } else {
                            // new interval, check last flat
                            if (flatTo != i - 1) {
                                flatFrom = flatTo = i - 1;
                            }

                            result[i] = flatFrom;

                            from = flatFrom;
                            flatFrom = -1;
                            flatTo = -1;
                        }

                        direction = 1;
                        break;
                    case -1:
                        result[i] = from;
                        direction = -1;
                        break;
                }

                i++;
            }

            return result;
        }
    }
}