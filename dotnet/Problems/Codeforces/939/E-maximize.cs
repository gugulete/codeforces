using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using Console = Utilities.Console;

namespace Contest_939 {

    static class Maximize {
        static List<long> values = new List<long> ();
        static List<long> counts = new List<long> ();
        static List<long> sums = new List<long> ();

        static CultureInfo culture = new CultureInfo ("en-US");

        static void Main1 (string[] args) {

            var sb = new StringBuilder ();
            var n = long.Parse (Console.ReadLine ());
            double result = 0;

            for (long i = 0; i < n; i++) {
                var q = Console.ReadLine ().Split (' ');
                if (q[0] == "1") {
                    var value = int.Parse (q[1]);
                    if (values.Count == 0) {
                        sums.Add (value);
                        values.Add (value);
                        counts.Add (1);
                    } else if (value == values[values.Count - 1]) {
                        var last = values.Count - 1;
                        sums[last] += value;
                        counts[last]++;
                    } else {
                        var last = values.Count - 1;

                        var mean = findMean (0, last, value);
                        result = value - mean;

                        values.Add (value);
                        sums.Add (sums[last] + value);
                        counts.Add (counts[last] + 1);
                    }
                } else {
                    sb.AppendLine (result.ToString (culture));
                }
            }

            Console.WriteLine (sb.ToString ());
        }

        static bool canAdd (int index, long value) {
            if (index == 0) {
                return true;
            }

            var n = values[index];
            var sum = sums[index - 1] + value;
            var count = counts[index - 1] + 1;

            return n * count <= sum;
        }

        static double findMean (int start, int end, int value) {
            // Console.WriteLine ($"{start} - {end}, {value}");

            int index = -1;
            if (start == end) {
                index = start;
            }

            if (end - start == 1) {
                index = canAdd (end, value) ? end : start;
            }

            if (index >= 0) {
                return (sums[index] + value) / (counts[index] + 1.0);
            }

            var mid = (start + end) / 2;

            if (canAdd (mid, value)) {
                return findMean (mid, end, value);
            } else {
                return findMean (start, mid - 1, value);
            }
        }
    }
}