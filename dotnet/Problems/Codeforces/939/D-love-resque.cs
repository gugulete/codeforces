using System;
using System.Collections.Generic;
using System.Text;
using Console = Utilities.Console;

namespace Contest_939 {

    static class LoveResque {
        static Dictionary<char, Node> letters = new Dictionary<char, Node> ();

        static void Main1 (string[] args) {
            var n = int.Parse (Console.ReadLine ());
            var tolya = Console.ReadLine ();
            var valya = Console.ReadLine ();

            for (int i = 0; i < n; i++) {
                if (tolya[i] != valya[i]) {
                    var t = GetNode (tolya[i]);
                    var v = GetNode (valya[i]);
                    
                    var tRoot = t.Root();
                    var vRoot = v.Root();

                    if (tRoot != vRoot) {
                        if (tRoot.Rank >= vRoot.Rank) {
                            tRoot.AddChild (vRoot);
                        } else {
                            vRoot.AddChild (tRoot);
                        }
                    }
                }
            }

            var count = 0;
            var sb = new StringBuilder ();
            foreach (var node in letters.Values) {
                if (node.Parent != null) {
                    count++;
                    sb.AppendLine ($"{node.Letter} {node.Parent.Letter}");
                }
            }

            sb.Insert (0, $"{count}\n");
            Console.WriteLine (sb.ToString ());
        }

        public static Node GetNode (char letter) {
            if (!letters.ContainsKey (letter)) {
                letters[letter] = new Node (letter);
            }

            return letters[letter];
        }
    }

    public class Node {
        public char Letter { get; }
        public int Rank { get; private set; }
        public Node Parent { get; private set; }
        
        public Node (char letter) {
            this.Letter = letter;
            this.Rank = 0;            
        }

        public void AddChild (Node node) {
            node.Parent = this;
            this.Rank = Math.Max (this.Rank, node.Rank + 1);
        }
        
        public Node Root() {
                var node = this;
                while (node.Parent != null) {
                    node = node.Parent;
                }
                return node;
        }
    }
}