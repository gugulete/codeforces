﻿using System;
using System.Collections.Generic;
using System.Linq;
using Console = Utilities.Console;

namespace Contest888 {

    static class ProblemG {
        static List<long> vertices;
        static Node tree;

        static void Main1 (string[] args) {
            var count = int.Parse (Console.ReadLine ());
            vertices = Console.ReadLine ().Split (' ').Select (long.Parse).ToList ();
            vertices.Sort ();

            tree = new Node {
                Left = 0,
                Right = count - 1,
                Bit = 30
            };

            Console.WriteLine (getWeight (tree));
        }

        static long getWeight (Node node) {
            if (node == null || node.Left >= node.Right || node.Bit < 0) {
                return 0;
            }

            int i = node.Left;
            while (i <= node.Right && ((vertices[i] >> node.Bit) & 1) == 0) {
                i++;
            }

            if (node.Left < i) {
                node.LeftNode = new Node {
                    Left = node.Left,
                    Right = i - 1,
                    Bit = node.Bit - 1
                };
            }

            if (i <= node.Right) {
                node.RightNode = new Node {
                Left = i,
                Right = node.Right,
                Bit = node.Bit - 1
                };
            }

            var leftWeight = getWeight (node.LeftNode);
            var rightWeight = getWeight (node.RightNode);

            long bridge = 0;
            if (node.LeftNode != null && node.RightNode != null) {
                bridge = xor (node.LeftNode, node.RightNode);
            }

            if (bridge == long.MaxValue) {
                Console.WriteLine ("shit");
                throw new Exception ();
            }

            return leftWeight + rightWeight + bridge;
        }

        private static long xor (Node leftNode, Node rightNode) {
            if (leftNode == null || rightNode == null) {
                return long.MaxValue;
            }

            var bit = Math.Min (leftNode.Bit, rightNode.Bit);

            if (bit < 0) {
                return vertices[leftNode.Left] ^ vertices[rightNode.Right];
            }

            if (leftNode.Left == leftNode.Right) {
                if (rightNode.Left == rightNode.Right) {
                    return vertices[leftNode.Left] ^ vertices[rightNode.Right];
                }

                var currentBit = (vertices[leftNode.Left] >> bit) & 1;
                if (currentBit == 0) {
                    return xor (leftNode, rightNode.LeftNode ?? rightNode.RightNode);
                } else {
                    return xor (leftNode, rightNode.RightNode ?? rightNode.LeftNode);
                }
            }

            if (rightNode.Left == rightNode.Right) {
                return xor (rightNode, leftNode);
            }

            var result = Math.Min (
                xor (leftNode.LeftNode, rightNode.LeftNode ),
                xor (leftNode.RightNode, rightNode.RightNode));


            if (result == long.MaxValue) {
                result = Math.Min (
                    xor (leftNode.LeftNode, rightNode.RightNode ),
                    xor (leftNode.RightNode, rightNode.LeftNode));
            }

            if (result == long.MaxValue) {
                Console.WriteLine ("shit 2");
                throw new Exception ();
            }

            return result;
        }
    }
}

class Node {
    public int Bit;
    public int Left;
    public int Right;
    public Node LeftNode;
    public Node RightNode;

    public override string ToString () {
        return $"[{Left} - {Right}]";
    }
}