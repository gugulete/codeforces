﻿using System;
using Console = Utilities.Console;

namespace Contest914 {

    static class ProblemA {

        static void Main1 (string[] args) {
            var count = Console.ReadLine ();

            var max = int.MinValue;

            foreach (var input in Console.ReadLine ().Split (' ')) {
                var n = int.Parse (input);
                if (!IsSquare (n)) {
                    if (n > max) {
                        max = n;
                    }
                }
            }

            Console.WriteLine (max);
        }

        static bool IsSquare (double n) {
            return Math.Sqrt (n) % 1 == 0;
        }
    }
}