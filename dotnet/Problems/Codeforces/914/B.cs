﻿using System;
using System.Collections.Generic;
using System.Linq;
using Console = Utilities.Console;

namespace Contest914 {

    static class ProblemB {

        static void Main1 (string[] args) {
            var _ = Console.ReadLine ();

            var stacks = Console.ReadLine ().Split (' ').Select (int.Parse).GroupBy (x => x).OrderBy (y => y.Key).ToList ();

            Console.WriteLine (WhoWins (stacks));
        }

        private static object WhoWins (List<IGrouping<int, int>> stacks) {
            for (int i = stacks.Count - 1; i >= 0; i--) {
                var stack = stacks[i];
                if (stack.Count () % 2 == 1) {
                    return "Conan";
                }
            }

            return "Agasa";
        }
    }
}