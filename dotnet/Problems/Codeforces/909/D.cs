using System;
using System.Collections.Generic;
using System.Linq;
using Console = Utilities.Console;

namespace Contest_909 {
    class Point {
        public int Count;
        public char Value;
        public Point (int count, char value) {
            this.Count = count;
            this.Value = value;
        }

        public override string ToString () {
            return $"{this.Value} {this.Count}";
        }
    }
    static class ColorfulPoints {
        static void Main1 (string[] args) {
            var input = Console.ReadLine ();
            var groups = new List<Point> ();

            var count = 0;
            var prevColor = input[0];

            foreach (var color in input) {
                if (color == prevColor) {
                    count++;
                } else {
                    groups.Add (new Point (count, prevColor));
                    count = 1;
                    prevColor = color;
                }
            }

            groups.Add (new Point (count, prevColor));

            var stages = 0;

            while (groups.Count > 1) {
                groups = MergeGroups (groups);
                stages++;
            }

            Console.WriteLine (stages);
        }

        private static List<Point> MergeGroups (List<Point> groups) {
            var newGroups = new List<Point> ();

            for (int i = 0; i < groups.Count; i++) {
                var group = groups[i];
                var count = group.Count - ((i == 0 || i == groups.Count - 1) ? 1 : 2);
                if (count > 0) {
                    var last = newGroups.LastOrDefault ();
                    if (last?.Value == group.Value) {
                        last.Count += count;
                    } else {
                        newGroups.Add (new Point (count, group.Value));
                    }
                }
            }

            return newGroups;
        }
    }
}