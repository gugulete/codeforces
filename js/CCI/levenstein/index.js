const utils = require('../../utilities');

const levensteinMatrix = (x, y) => {
    const matrix = utils.array2D(x.length, y.length);

    const topValue = (i, j) => {
        if (i === 0) {
            return j + 1;
        } else {
            return matrix[i - 1][j];
        }
    }

    const leftValue = (i, j) => {
        if (j === 0) {
            return i + 1;
        } else {
            return matrix[i][j - 1];
        }
    }

    const diagAboveValue = (i, j) => {
        if (i === 0) {
            if (j === 0) {
                return 0;
            }

            return topValue(i, j - 1);
        } else {
            return leftValue(i - 1, j);
        }
    }

    const increment = (i, j) => x[i] === y[j] ? 0 : 1;

    for (let i = 0; i < x.length; i++) {
        for (let j = 0; j < y.length; j++) {
            const top = topValue(i, j);
            const left = leftValue(i, j);
            const diag = diagAboveValue(i, j);
            const inc = increment(i, j);
            matrix[i][j] = Math.min(Math.min(top, left), diag) + inc;
        }
    }

    return matrix;
};


module.exports = () => {
    const x = readline();
    const y = readline();

    const matrix = levensteinMatrix(x, y);
    utils.prinatArray2D(matrix);
};