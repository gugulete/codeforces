'use strict';

const read = () => {
    const nodes = {};
    const root = {};

    const [x, y] = readline().split(' ');
    nodes[parseInt(readline())] = root;

    let line;
    while (line = readline()) {
        const [p, l, r] = line.split(' ').map(x => parseInt(x));
        if (!nodes[p]) {
            nodes[p] = {
                value: p
            };
        }

        if (!isNaN(l)) {
            if (!nodes[l]) {
                nodes[l] = {
                    value: l
                };
            }

            nodes[p].left = nodes[l];
        }

        if (!isNaN(r)) {
            if (!nodes[r]) {
                nodes[r] = {
                    value: r
                };
            }

            nodes[p].right = nodes[r];
        }
    }

    return {
        root,
        x,
        y
    };
};

const findFCA = (node, x, y) => {
    if (!node) {
        return {
            x: false,
            y: false
        };
    }

    if (node.value === x) {
        return {
            x: true,
            y: false
        };
    }

    if (node.value === y) {
        return {
            x: false,
            y: true
        };
    }

    const left = findFCA(node.left, x, y);
    const right = findFCA(node.right, x, y);

    const result = {
        x: left && left.x || right && right.x,
        y: left && left.y || right && right.y
    };

    return result;
};



module.exports = () => {
    try {
        const {
            root,
            x,
            y
        } = read();

        console.log(findFCA(root, x, y, {
            foundX: false,
            foundY: false
        }));
    } catch (error) {
        console.log(error);
    }
};