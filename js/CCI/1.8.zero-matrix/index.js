'use strict';

const utils = require('../../utilities');

module.exports = () => {
    let rows = 0;
    let cols = 0;

    const matrix = [];

    let line = readline();
    while (line) {
        const elements = line.split(' ').map(s => parseInt(s));
        matrix.push(elements);
        rows++;
        cols = elements.length;
        line = readline();
    }

    const zeroRows = new Set();
    const zeroCols = new Set();

    for (let row = 0; row < rows; row++) {
        for (let col = 0; col < cols; col++) {
            if (matrix[row][col] === 0) {
                zeroRows.add(row);
                zeroCols.add(col);
            }
        }
    }

    for (let row of zeroRows) {
        for (let col = 0; col < cols; col++) {
            matrix[row][col] = 0;
        }
    }

    for (let col of zeroCols) {
        for (let row = 0; row < rows; row++) {
            matrix[row][col] = 0;
        }
    }

    utils.prinatArray2D(matrix);
};