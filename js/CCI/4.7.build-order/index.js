'use strict';

const readDependencies = () => {
    const nodes = {};
    const projects = readline().split(' ');
    let dependencies = parseInt(readline());

    while (dependencies > 0) {
        dependencies--;

        let [a, b] = readline().split(' ');
        if (!nodes[a]) {
            nodes[a] = {
                dependencies: new Set()
            };
        }

        if (!nodes[b]) {
            nodes[b] = {
                dependencies: new Set()
            };
        }

        nodes[a].dependencies.add(b);
    }

    return nodes;
};

const build = (nodes, key, order) => {
    const node = nodes[key];
    if (node.built) {
        return;
    }

    if (node.traversed) {
        throw Error('circular rependency found');
    }

    node.traversed = true;
    node.dependencies && node.dependencies.forEach(dep => {
        build(nodes, dep, order);
    });

    node.built = true;
    order.push(key);
};

module.exports = () => {
    try {
        const nodes = readDependencies();
        const order = [];

        Object.keys(nodes).forEach(key => {
            build(nodes, key, order);
        });

        console.log(order.join(' '));
    } catch (error) {
        console.log(error);
    }
};