'use strict';

const BST = require("../../structures/bst");

module.exports = () => {
    const array = readline().split(' ').map(a => parseInt(a));
    const tree = BST.fromArray(array);

    const paths = [];
    const generatePaths = (path, options) => {
        if (options.size === 0) {
            paths.push(path.join(' '));
        } else {
            const currentOptions = [...options];
            currentOptions.forEach(node => {
                options.delete(node);
                node.left && options.add(node.left);
                node.right && options.add(node.right);
                path.push(node.value);

                generatePaths(path, options);

                node.left && options.delete(node.left);
                node.right && options.delete(node.right);
                options.add(node);
                path.pop();
            });
        }
    };

    generatePaths([], new Set([tree.root]));
    console.log(paths.join('\n'));
    console.log(paths.length);
};