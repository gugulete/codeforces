'use strict';

const utils = require('../../utilities');

module.exports = () => {
    let head = null;
    let previous = null;

    const getElement = (value) => {
        return {
            value,
            next: null
        };
    };

    const elements = readline().split('');
    elements.forEach(element => {
        if (head) {
            previous.next = getElement(element);
            previous = previous.next;
        } else {
            head = getElement(element);
            previous = head;
        }
    });


    const isPalindrome = (result, length) => {
        if (!result.isPalindrome) {
            return result;
        }

        if (!result.node || length == 0) {
            return {
                node: result.node,
                isPalindrome: true
            };
        }

        if (length == 1) {
            return {
                node: result.node.next,
                isPalindrome: true
            };
        }

        const temp = isPalindrome({
            node: result.node.next,
            isPalindrome: true
        }, length - 2);

        return {
            isPalindrome: temp.isPalindrome && (temp.node.value === result.node.value),
            node: temp.node.next
        };
    }

    console.log(isPalindrome({
        node: head,
        isPalindrome: true
    }, elements.length).isPalindrome);
};