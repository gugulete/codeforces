'use strict';

const readTree = () => {
    const nodes = {};
    const root = {};

    const n = parseInt(readline());
    nodes[parseInt(readline())] = root;

    for (let i = 0; i < n; i++) {
        const [p, l, r] = readline().split(' ').map(x=>parseInt(x));
        if (!nodes[p]) {
            nodes[p] = {};
        }

        if (!isNaN(l)) {
            if (!nodes[l]) {
                nodes[l] = {};
            }

            nodes[p].left = nodes[l];
        }

        if (!isNaN(r)) {
            if (!nodes[r]) {
                nodes[r] = {};
            }

            nodes[p].right = nodes[r];
        }
    }

    return root;
};

const isBalanced = (node) => {
    if (!node) {
        return true;
    }

    const params = {
        min: Number.MAX_SAFE_INTEGER,
        max: Number.MIN_SAFE_INTEGER
    };

    return traverse(node, params, 0);
}

const traverse = (node, params, depth) => {
    if (!node) {
        return true;
    }

    if (!node.left || !node.right) {
        if (depth < params.min) {
            params.min = depth;
        }

        if (depth > params.max) {
            params.max = depth;
        }

        if (Math.abs(params.min - params.max) > 1) {
            return false;
        }
    }

    return traverse(node.left, params, depth + 1) &&
        traverse(node.right, params, depth + 1);
}

module.exports = () => {
    const root = readTree();
    console.log(isBalanced(root));
};