'use strict';

const readTree = () => {
    const nodes = {};

    const n = parseInt(readline());
    const root = {
        value: parseInt(readline())
    };

    nodes[root.value] = root;

    for (let i = 0; i < n; i++) {
        const [p, l, r] = readline().split(' ').map(x => parseInt(x));
        if (!nodes[p]) {
            nodes[p] = {
                value: p
            };
        }

        if (!isNaN(l)) {
            if (!nodes[l]) {
                nodes[l] = {
                    value: l
                };
            }

            nodes[p].left = nodes[l];
        }

        if (!isNaN(r)) {
            if (!nodes[r]) {
                nodes[r] = {
                    value: r
                };
            }

            nodes[p].right = nodes[r];
        }
    }

    return root;
};

const isBST = (node, min, max) => {
    if (!node) {
        return true;
    }

    if (node.value >= max || node.value < min) {
        return false;
    }

    if (node.left && node.left.value > node.value) {
        return false;
    }
    if (node.right && node.right.value <= node.value) {
        return false;
    }

    return isBST(node.left, min, node.value) &&
        isBST(node.right, node.value, max);
}

module.exports = () => {
    const root = readTree();
    console.log(isBST(root, Number.MIN_SAFE_INTEGER, Number.MAX_SAFE_INTEGER));
};