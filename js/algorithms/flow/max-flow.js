module.exports = () => {
    const input = [
        // `0 1 16`,
        // `0 2 13`,
        // `1 2 10`,
        // `2 1 4`,
        // `1 3 12`,
        // `2 4 14`,
        // `3 2 9`,
        // `4 5 4`,
        // `4 3 7`,
        // `3 5 20`
        `0 1 1`,
        `0 2 1`,
        `0 3 1`,
        `1 2 1`,
        `2 3 1`,
        `2 6 1`,
        `3 6 1`,
        `4 2 1`,
        `4 7 1`,
        `5 1 1`,
        `5 4 1`,
        `5 7 1`,
        `6 5 1`,
        `6 7 1`
    ];

    const verticesCount = 8;
    const source = 0;
    const target = 7;

    const vertices = [...Array(verticesCount)].map(() => {
        return {
            flow: 0,
            prev: {}
        };
    });

    input.forEach(line => {
        const params = line.split(' ');
        const from = parseInt(params[0]);
        const to = parseInt(params[1]);
        const flow = parseInt(params[2]);

        vertices[to].prev[from] = flow;
    });

    function addFlow(current, vertices, source) {
        const vertex = vertices[current];
        vertex.traversed = true;

        if (current === source) {
            vertex.flow++;

            vertex.traversed = false;
            return true;
        }

        for (let v in vertex.prev) {
            const prev = parseInt(v);
            if (vertices[prev].traversed) {
                continue;
            }

            if (addFlow(prev, vertices, source)) {
                vertex.prev[v]--;
                if (vertex.prev[v] === 0) {
                    delete vertex.prev[v];
                }

                vertex.flow++;
                vertex.traversed = false;
                return true;
            }
        }

        vertex.traversed = false;
        return false;
    }

    while (addFlow(target, vertices, source)) {}

    console.log(vertices[target].flow);
};