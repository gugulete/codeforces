const problemPath = './CCI/4.9.tree-paths';


const path = require('path');
const fs = require('fs');

const problem = require(problemPath);
const input = fs.readFileSync(path.resolve(__dirname, problemPath, './input.txt'), 'utf8');

function* inputGenerator() {
    const lines = input.split(`\n`);
    for (let i = 0; i < lines.length; i++) {
        yield lines[i];
    }

    yield null;
};

global.print = s => console.log(s);

const run = () => {
    const readInput = inputGenerator();

    if (input.startsWith('tests: ')) {
        const tests = readInput.next().value;
        const testsCount = parseInt(tests.substring(7).trim());
        if (isNaN(testsCount)) {
            throw Error('invalid tests count');
        }

        for (let i = 0; i < testsCount; i++) {
            global.readline = () => readInput.next().value;
            problem();
        }
    } else {
        global.readline = () => readInput.next().value;
        problem();
    }
}

run();