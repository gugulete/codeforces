const input = readline().split(' ').map(v => parseInt(v));

const n = input[0];
const m = input[1];
const v = input[2];

function output(s) {
    print(s);
}

function comb(n, k) {
    var f = 1;
    for (var i = k; i >= 1; i--) {
        f = f * (n - i + 1) / (k - i + 1);
    }
    return f;
}

function connect(servers, connections, special) {
    for (var i = 1; i < servers; i++) {
        for (var j = i + 1; j <= servers; j++) {
            var left = i;
            var right = j;

            if (i === 1) {
                left = special;
            } else if (i === special) {
                left = servers + 1;
            }

            if (j === special) {
                right = servers + 1;
            }

            output(left + ' ' + right);
            connections--;
            if (connections === 0) {
                return;
            }
        }
    }
}

if (m < n - 1 || comb(n - 1, 2) < m - 1) {
    output(-1);
} else {
    connect(n - 1, m - 1, v);
    output(v + ' ' + (v === 1 ? n : 1));
}
