const count = readline();
const claws = readline().split(' ').map(v=> parseInt(v));

var alive = 0;
var lastAlive = claws.length - 1;

for (var killer = claws.length - 1; killer >= 0; killer--) {
    print(lastAlive);
    
    if (killer === lastAlive) {
        alive++;
    }

    lastAlive = Math.min(lastAlive, killer - claws[killer] - 1);
}

print(alive);