module.exports = (count, line) => {
    // const count = parseInt(readline());
    // const line = readline();

    const probabilities = line.split(' ').map(v => parseFloat(v));
    const result = { result: 0 };

    function output(s) {
        // print(s);
        console.log(s);
    }

    function calculateStuff(length, lastSeq, score, prob) {

        if (prob === 0) {
            return;
        }

        if (length === count) {
            result.result += score * prob;
            return;
        }

        add_0(length, lastSeq, score, prob);
        add_1(length, lastSeq, score, prob);
    }

    function add_0(length, lastSeq, score, prob) {
        lastSeq = 0;
        prob *= 1 - probabilities[length];
        length++;

        return calculateStuff(length, lastSeq, score, prob);
    }

    function add_1(length, lastSeq, score, prob) {
        score += 2 * lastSeq + 1;
        lastSeq++;
        prob *= probabilities[length];
        length++;

        return calculateStuff(length, lastSeq, score, prob);
    }

    calculateStuff(0, 0, 0, 1);
    output(result.result);
};