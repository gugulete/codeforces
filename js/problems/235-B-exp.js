module.exports = (count, line) => {
    // const count = parseInt(readline());
    // const line = readline();

    function output(s) {
        // print(s);
        console.log(s);
    }

    const probabilities = line.split(' ').map(v => parseFloat(v));
    const sequence = new Array(count + 1);

    // init
    sequence[count] = {
        value: 0,
        score: 0,
        prob: 1,
        ones: 0
    };

    for (var i = count - 1; i >= 0; i--) {
        sequence[i] = {
            value: 0,
            score: 0,
            prob: sequence[i + 1].prob * (1 - probabilities[i]),
            ones: 0
        };
    }
    // end init

    var result = 0;
    var first = sequence[0];

    while (true) {
        // output(sequence.map(s => s.value));
        // output(sequence.map(s => s.ones));
        // output('-----');

        var backIndex = first.ones;

        if (backIndex === count) {
            break;
        }

        var next = sequence[backIndex + 1];

        sequence[backIndex].value = 1;
        sequence[backIndex].score = next.score + 2 * next.ones + 1;
        sequence[backIndex].prob = next.prob * probabilities[backIndex];
        sequence[backIndex].ones = next.ones + 1;

        for (var i = backIndex - 1; i >= 0; i--) {
            sequence[i].value = 0;
            sequence[i].ones = 0;
            sequence[i].score = sequence[i + 1].score;
            sequence[i].prob = sequence[i + 1].prob * (1 - probabilities[i]);
        }

        result += first.prob * first.score;
    }

    output(result);
};