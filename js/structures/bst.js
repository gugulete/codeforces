'use strict';

/** Binary search tree */
class BST {
    get root() {
        return this._root;
    }

    constructor() {
        this._root = null;
    }

    static fromArray(array) {
        if (!array) {
            throw Error('no array');
        }

        if (!Array.isArray(array)) {
            throw Error('array is not array');
        }

        const bst = new BST();
        array.forEach(element => bst.insert(element));

        return bst;
    }

    insert(value) {
        this._root = this._insert(value, this._root);
    }

    _insert(value, current) {
        if (!current) {
            return new Node(value);
        }

        if (value > current.value) {
            current.right = this._insert(value, current.right);
        } else {
            current.left = this._insert(value, current.left);
        }

        return current;
    }

    _inOrder(node, func) {
        if (!node) {
            return;
        }

        this._inOrder(node.left, func);
        func(node);
        this._inOrder(node.right, func);
    }

    inOrderArray() {
        const values = [];
        this._inOrder(this._root, node => values.push(node.value));
        return values;
    }
}

class Node {
    constructor(value) {
        this.left = null;
        this.right = null;
        this.value = value;
    }
}

module.exports = BST;