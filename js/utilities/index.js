'use strict';

const zeroGenerator = () => 0;

const array = (length, elementGenerator = zero) => [...new Array(length)].map((_1, index) => elementGenerator(index));

const array2D = (length1, length2, elementGenerator = zeroGenerator) => [...new Array(length1)].map((_, index1) => [...new Array(length2)].map((_2, index2) => elementGenerator(index1, index2)));

const prinatArray2D = (array) => {
  console.log(array.map(row => row.join(' ')).join('\n'));
}

module.exports = {
    array,
    array2D,
    prinatArray2D
}