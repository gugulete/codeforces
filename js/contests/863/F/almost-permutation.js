module.exports = () => {

//     `3 2
// 1 1 3 2
// 2 1 3 2`

    // read input
    var params = readline().split(' ');
    var arrayLength = parseInt(params[0]);

    var array = Array.apply(null, Array(arrayLength)).map(() => {
        return {
            min: 1,
            max: arrayLength
        };
    });

    var result;

    var factsCount = parseInt(params[1]);
    for (var i = 0; i < factsCount; i++) {
        var factParams = readline().split(' ');

        var type = factParams[0];
        var start = parseInt(factParams[1]) - 1;
        var end = parseInt(factParams[2]) - 1;
        var value = parseInt(factParams[3]);

        for (var i = start; i <= end; i++) {
            if (type === '1') {
                array[i].min = Math.max(value, array[i].min);
            } else {
                array[i].max = Math.min(value, array[i].max);
            }

            if (array[i].max < array[i].min) {
                result = -1;
                break;
            }
        }

        if (result) {
            break;
        }
    }

    // end read input
    if (result) {
        print(result);
    } else {

        print(output.join(' '));





    }
};