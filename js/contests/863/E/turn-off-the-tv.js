module.exports = () => {
    function between(x, start, end) {
        return x >= start && x <= end;
    }

    function intersects(start1, end1, start2, end2) {
        return between(start1, start2, end2) ||
            between(end1, start2, end2) ||
            between(start2, start1, end1);
    }

    function addRange(tvStart, tvEnd, nodeStart, nodeEnd, nodeIndex, tree) {
        if (tvStart === nodeStart && tvEnd === nodeEnd) {
            tree[nodeIndex] = (tree[nodeIndex] || 0) + 1;
            // treeMin[nodeIndex] = tree[nodeIndex];
        } else {
            var leftChildStart = nodeStart;
            var leftChildEnd = (nodeEnd + nodeStart - 1) / 2;

            if (intersects(tvStart, tvEnd, leftChildStart, leftChildEnd)) {
                addRange(Math.max(tvStart, leftChildStart), Math.min(tvEnd, leftChildEnd), leftChildStart, leftChildEnd, nodeIndex * 2 + 1, tree);
            }

            var rightChildStart = (nodeEnd + nodeStart + 1) / 2;
            var rightChildEnd = nodeEnd;
            if (intersects(tvStart, tvEnd, rightChildStart, rightChildEnd)) {
                addRange(Math.max(tvStart, rightChildStart), Math.min(tvEnd, rightChildEnd), rightChildStart, rightChildEnd, nodeIndex * 2 + 2, tree);
            }
        }
    }

    function processTree(nodeIndex, parentCount, tree) {
        if (nodeIndex >= tree.length) {
            return;
        }

        tree[nodeIndex] = (tree[nodeIndex] || 0) + parentCount;

        processTree(nodeIndex * 2 + 1, tree[nodeIndex], tree);
        processTree(nodeIndex * 2 + 2, tree[nodeIndex], tree);
    }

    function processTreeMin(nodeIndex, tree, treeMin) {
        if (nodeIndex * 2 + 2 < tree.length) {
            var left = processTree2(nodeIndex * 2 + 1, tree, treeMin);
            var right = processTree2(nodeIndex * 2 + 2, tree, treeMin);
            treeMin[nodeIndex] = Math.min(left, right);
        } else {
            treeMin[nodeIndex] = tree[nodeIndex] || 0;
        }

        return treeMin[nodeIndex];
    }

    function arrayMin(array, start, end) {
        return array.slice(start, end + 1).reduce(function (a, b) {
            return Math.min(a, b);
        });
    }

    // read input
    var count = parseInt(readline());

    var tvs = [];
    var tvTimes = {};
    for (var i = 0; i < count; i++) {
        var input = readline().split(' ');

        var tv = {
            start: parseInt(input[0]),
            end: parseInt(input[1])
        };

        tvs.push(tv);

        tvTimes[tv.start] = 0;
        tvTimes[tv.end] = 0;
        tvTimes[tv.end + 1] = 0;
    }

    var times = Object.keys(tvTimes);

    var timesMap = {};
    times.map((t, i) => timesMap[t] = i);

    var power = Math.ceil(Math.log2(times.length));
    var intervalsLength = 1 << power;

    var treeLength = 0;
    for (var i = 0; i <= power; i++) {
        treeLength = (treeLength << 1) + 1;
    }

    var tree = Array(treeLength);
    var treeMin = Array(treeLength);

    tvs.forEach(tv => {
        addRange(timesMap[tv.start], timesMap[tv.end], 0, intervalsLength - 1, 0, tree);
    });

    processTree(0, 0, tree);
    tree.splice(0, tree.length - intervalsLength);

    function tvIndex(tvs) {
        for (var i = 0; i < tvs.length; i++) {
            var tv = tvs[i];
            if (arrayMin(tree, timesMap[tv.start], timesMap[tv.end]) > 1) {
                return i + 1;
            }
        }

        return -1;
    }

    print(tvIndex(tvs));
};