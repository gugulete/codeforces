module.exports = () => {

    var playScores = [
        [0, 1, -1],
        [-1, 0, 1],
        [1, -1, 0]
    ];

    function strip(array) {
        while (!array[0] && array.length > 1) {
            array.shift();
        }

        return array;
    }

    function toArray(str) {
        var r = [];
        for (var i = 0; i < str.length; i++) {
            r.push(parseInt(str[i]));
        }

        return r;
    }

    function subtractDigit(x, y) {
        var result = x.map(e => e);
        var i = x.length - 1;
        result[i] = result[i] - y;

        if (result[i] < 0) {
            result[i] += 10;
            var j = i - 1;
            while (j >= 0 && !result[j]) {
                result[j] = 9;
                j--;
            }

            result[j]--;
        }

        return strip(result);
    }

    function divide(x, y) {
        var result = [];
        var remainder = 0;
        for (var i = 0; i < x.length; i++) {
            var current = x[i] + 10 * remainder;

            remainder = current % y;
            result.push((current - remainder) / y);
        }

        strip(result);
        return {
            result,
            remainder
        };
    }

    function multiply(x, y) {
        var result = [];
        var carry = 0;

        for (var i = x.length - 1; i >= 0; i--) {
            var r = x[i] * y + carry;
            carry = Math.floor(r / 10);
            result.unshift(r % 10);
        }

        while (carry) {
            result.unshift(carry % 10);
            carry = Math.floor(carry / 10);
        }

        return strip(result);
    }

    function add(x, y) {
        var result = [];

        for (var i = x.length - 1; i >= 0; i--) {
            if (y) {
                var r = x[i] + y;
                y = Math.floor(r / 10);
                result.unshift(r % 10);
            } else {
                result.unshift(x[i]);
            }
        }

        while (y) {
            result.unshift(y % 10);
            y = Math.floor(y / 10);
        }

        return strip(result);
    }

    // read input
    var gameParams = readline().split(' ');

    var gamesToPlay = parseInt(gameParams[0]);
    var aliceStart = parseInt(gameParams[1]);
    var bobStart = parseInt(gameParams[2]);

    var gameStages = [];

    for (var i = 0; i < 3; i++) {
        gameStages.push([]);
        var line = readline().split(' ');
        for (var j = 0; j < line.length; j++) {
            gameStages[i].push({
                playedGames: 0,
                alice: parseInt(line[j])
            });
        }
    }

    for (var i = 0; i < 3; i++) {
        var line = readline().split(' ');
        for (var j = 0; j < line.length; j++) {
            var stage = gameStages[i][j];
            stage.bob = parseInt(line[j]);

            var score = playScores[stage.alice - 1][stage.bob - 1];
            stage.score = {
                alice: -Math.min(0, score),
                bob: Math.max(0, score)
            };
        }
    }

    // play
    var firstScore = playScores[aliceStart - 1][bobStart - 1];

    var stage = gameStages[aliceStart - 1][bobStart - 1];
    stage.accumulatedScore = {
        alice: -Math.min(0, firstScore),
        bob: Math.max(0, firstScore)
    };

    var accumulatedScore = stage.accumulatedScore;
    var playedGames = 1;

    while (playedGames < gamesToPlay && !stage.playedGames) {
        // play stage
        stage.playedGames = playedGames;
        stage.accumulatedScore = accumulatedScore;

        accumulatedScore = {
            alice: accumulatedScore.alice + stage.score.alice,
            bob: accumulatedScore.bob + stage.score.bob
        };

        stage = gameStages[stage.alice - 1][stage.bob - 1];
        playedGames++;
    }

    var finalScore;
    if (playedGames === gamesToPlay) {
        finalScore = accumulatedScore;
    } else {
        finalScore = {
            alice: stage.accumulatedScore.alice,
            bob: stage.accumulatedScore.bob
        };

        var loopGames = playedGames - stage.playedGames;

        if (gamesToPlay <= Number.MAX_SAFE_INTEGER) {
            gamesToPlay -= stage.playedGames;

            var beforeLoopScore = stage.accumulatedScore;
            var loopScore = {
                alice: accumulatedScore.alice - stage.accumulatedScore.alice,
                bob: accumulatedScore.bob - stage.accumulatedScore.bob
            };

            var reps = Math.floor(gamesToPlay / loopGames);
            finalScore.alice += loopScore.alice * reps;
            finalScore.bob += loopScore.bob * reps;

            var rem = gamesToPlay % loopGames;
            while (rem >= 1) {
                if (rem === 1) {
                    finalScore.alice += stage.accumulatedScore.alice - beforeLoopScore.alice + stage.score.alice;
                    finalScore.bob += stage.accumulatedScore.bob - beforeLoopScore.bob + stage.score.bob;
                    break;
                } else {
                    stage = gameStages[stage.alice - 1][stage.bob - 1];
                    rem--;
                }
            }
        } else {
            var gamesToPlayBig = subtractDigit(toArray(gameParams[0]), stage.playedGames);

            var beforeLoopScore = stage.accumulatedScore;
            var loopScore = {
                alice: accumulatedScore.alice - stage.accumulatedScore.alice,
                bob: accumulatedScore.bob - stage.accumulatedScore.bob
            };

            var reps = divide(gamesToPlayBig, loopGames);

            finalScore.alice = add(multiply(reps.result, loopScore.alice), finalScore.alice);
            finalScore.bob = add(multiply(reps.result, loopScore.bob), finalScore.bob);


            while (reps.remainder >= 1) {
                if (reps.remainder === 1) {
                    finalScore.alice = add(finalScore.alice, stage.accumulatedScore.alice - beforeLoopScore.alice + stage.score.alice);
                    finalScore.bob = add(finalScore.bob, stage.accumulatedScore.bob - beforeLoopScore.bob + stage.score.bob);
                    break;
                } else {
                    stage = gameStages[stage.alice - 1][stage.bob - 1];
                    reps.remainder--;
                }
            }

            finalScore.alice = finalScore.alice.join('');
            finalScore.bob = finalScore.bob.join('');
        }
    }

    print(finalScore.alice + ' ' + finalScore.bob);
};