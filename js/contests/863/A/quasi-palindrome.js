module.exports = (line) => {
    var line = readline();

    function isQuasi(input) {
        var end = input.length - 1;
        while (end >= 0 && input[end] === '0') {
            end--;
        }

        var start = 0;
        while (start < end) {
            if (input[start] !== input[end]) {
                return 'NO';
            }

            start++;
            end--;
        }

        return 'YES';
    }

    print(isQuasi(line));
};