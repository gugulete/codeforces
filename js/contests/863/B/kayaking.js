module.exports = () => {
    var count = readline();
    var sorted = readline().split(' ').map(s => parseInt(s)).sort((a, b) => {
        return a - b;
    });

    var minSum = 25000;

    for (var first = 0; first < sorted.length; first++) {
        for (var second = first + 1; second < sorted.length; second++) {
            var sum = 0;
            var i = 0;
            while (i < sorted.length) {
                if (i === first || i === second) {
                    i++;
                    continue;
                }

                var next = i + 1;
                while (next === first || next === second) {
                    next++;
                }

                sum += sorted[next] - sorted[i];
                if (sum >= minSum) {
                    break;
                }

                i = next + 1;
            }

            if (sum < minSum) {
                minSum = sum;
            }
        }
    }

    print(minSum);
};