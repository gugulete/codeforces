module.exports = () => {
    function reverse(x, start, end) {
        if (x < start || x > end) {
            return x;
        }

        var mid = (start + end) / 2;
        if (x === mid) {
            return x;
        }

        var dist = mid - x;
        return mid + dist;
    }

    function unshift(x, start, end) {
        if (x < start || x > end) {
            return x;
        }

        if (x === start) {
            return end;
        }

        return x - 1;
    }

    // read input
    var params = readline().split(' ');
    var queriesCount = parseInt(params[1]);

    var elements = readline().split(' ');
    elements.unshift('0');

    var queries = [];
    for (var i = 0; i < queriesCount; i++) {
        var queryParams = readline().split(' ');
        queries.push({
            type: parseInt(queryParams[0]),
            start: parseInt(queryParams[1]),
            end: parseInt(queryParams[2])
        });
    }

    var special = readline().split(' ');
    // end read input

    var output = [];
    for (var i = 0; i < special.length; i++) {
        var index = parseInt(special[i]);

        for (var q = queries.length - 1; q >= 0; q--) {
            var query = queries[q];
            index = query.type === 1 ? unshift(index, query.start, query.end) : reverse(index, query.start, query.end);
        }

        output.push(elements[index]);
    }

    print(output.join(' '));
};