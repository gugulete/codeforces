module.exports = () => {

    function isLeaf(node) {
        return Object.keys(node.children).length === 0;
    }

    // input
    var n = parseInt(readline());

    var nodes = new Array(n);
    nodes[0] = {
        isLeaf: true,
        leaves: 0,
        parent: null
    };

    for (var i = 1; i < n; i++) {
        var parentIndex = parseInt(readline()) - 1;
        if (!nodes[i]) {
            // init current node
            nodes[i] = {
                isLeaf: true,
                leaves: 0,
                parent: parentIndex
            };
        }

        var currentNode = nodes[i];

        if (!nodes[parentIndex]) {
            // init parent node
            nodes[parentIndex] = {
                isLeaf: false,
                leaves: currentNode.isLeaf ? 1 : 0,
                parent: null
            };
        } else {
            // update parent node
            var parentNode = nodes[parentIndex];
            if (parentNode.isLeaf) {
                parentNode.isLeaf = false;
                var parentParentNode = nodes[parentNode.parent];
                if (parentParentNode) {
                    parentParentNode.leaves = Math.max(0, parentParentNode.leaves - 1);
                }
            }

            if (currentNode.isLeaf) {
                parentNode.leaves++;
            }
        }
    }

    function isSpruce(tree) {
        for (var i = 0; i < n; i++) {
            var node = tree[i];
            if (!node.isLeaf && node.leaves < 3) {
                return 'No';
            }
        }
        return 'Yes';
    }

    print(isSpruce(nodes));
};