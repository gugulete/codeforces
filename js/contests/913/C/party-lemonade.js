module.exports = () => {

    function arrayMin(array, start, end) {
        return array.slice(start, end).reduce(function (a, b) {
            return Math.min(a, b);
        });
    }

    var line = readline().split(' ');

    var n = parseInt(line[0]);
    var L = parseInt(line[1]);

    var costs = readline().split(' ').map(c => parseInt(c));
    var arr = Array(n);

    arr[0] = costs[0];
    var prevCost = arr[0];

    var maxn = Math.ceil(Math.log2(L));

    for (var i = 1; i < n; i++) {
        if (i < maxn) {
            arr[i] = Math.min(costs[i], 2 * prevCost);
        } else if (i === maxn) {
            var prev = 1 << (i - 1);
            var rem = L - prev;

            if (rem === prev) {
                arr[i] = Math.min(costs[i], 2 * prevCost);
            } else {
                var start = Math.ceil(Math.log2(rem));
                var min = prevCost + arrayMin(arr, start, i);
                arr[i] = Math.min(costs[i], min);
            }
        } else { // (i > maxn)
            arr[i] = Math.min(costs[i], prevCost);
        }

        prevCost = arr[i];
    }

    // print(arr);

    var cost = 0;
    // print(`L: ${L}`);
    // print(`-----`);
    for (var j = n - 1; j >= 0; j--) {
        var count = L >> j;
        if (count === 0) {
            cost += arr[j];
            // print(`buy ${Math.pow(2,j)}: ${count}`);
            break;
        }

        // print(`count ${Math.pow(2,j)}: ${count}`)
        L -= count << j;
        // print(`remaining: ${L}`);
        cost += count * arr[j];
        // print(`c[${Math.pow(2,j)}]: ${arr[j]}`);
        // print(`cost: ${cost}`);
        // print(`-----`);

        if (L === 0) {
            break;
        }
    }

    print(cost);
    print('44981600785557577');
    print(Number.MAX_SAFE_INTEGER);
    print('9223372036854775807');
};