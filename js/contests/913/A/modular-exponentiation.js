module.exports = () => {
    var n = parseInt(readline());
    var m = parseInt(readline());

    var max = Math.floor(Math.log2(m));

    if (n > max) {
        print(m);
    } else {
        var power = Math.pow(2, n);
        print(m % power);
    }
};